FROM ubuntu:20.04

LABEL maintainer="Victoria Gur <v.gur@f5.com>"

RUN pip3 install -r requirements.txt
RUN echo 'export PATH=/.local/bin:$PATH'

ENV PATH /.local/bin:$PATH

WORKDIR /assignment

COPY . .

CMD ["PYTHON3", "./tests/string_found.py"]
CMD ["PYTHON3", "./tests/string_not_found.py"]
