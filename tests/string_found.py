import requests
import html5lib
import re
from bs4 import BeautifulSoup

def test_string_found():
    url = 'http://the-internet.herokuapp.com/context_menu'
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html5lib')
    assert soup.find(string=re.compile("Right-click in the box below to see one called \'the-internet\'"))
