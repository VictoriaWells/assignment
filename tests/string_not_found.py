import requests
import html5lib
import re
from bs4 import BeautifulSoup

class MyError(Exception):
    def __init__(self, m):
        self.m = m
    def __str__(self):
        return self.m

def test_string_found():
    url = 'http://the-internet.herokuapp.com/context_menu'
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html5lib')
    try:
        assert (soup.find(string=re.compile("Alibaba"))), "String Alibaba wasn't found on page."
    except MyError:
        pytest.fail("Failed to execute test.")
