# Assignment

The assignment for NAP
 
#Tracking steps for assignment
 
During the assignment I wrote down the steps that were taken. In some of them I will explain why I chose this approach and how I understood the task.
 
#1. Create VM
In open stack I created an instance of Waf_Ubuntu_1 from an image named  mBIP-ubuntu-old mainly because I need a steady build for a newer version of python than I had on my Ubuntu 16.04. The articles that explain how to upgrade didn’t work in our env. So the fastest way was to create a VM with all needed basics.
 
vagrant@10.240.105.11 
 
Distributor ID: Ubuntu
Description:    Ubuntu 20.04 LTS
Release:        20.04
Codename:       focal
 
python3 --version
Python 3.8.10
 
 
—----------------------------------------------------------------------------------------------------------

#2. Install venv
 
I choose to work in a virtual environment that allows me to manage separate package installations.
 
pip3 --version
pip 20.0.2 from /usr/lib/python3/dist-packages/pip (python 3.8)
 
sudo apt-get update  
sudo pip3 install virtualenv 
Requirement already satisfied: virtualenv in /usr/lib/python3/dist-packages (20.0.17)
 
Create virtual environment
mkdir assignment
cd assignment
virtualenv -p python3 assignment
 
Activate virtual environment
source assignment/bin/activate
(assignment) vagrant@waf-ubuntu-1:~/assignment$
—----------------------------------------------------------------------------------------------------------

#3. Install pytest
sudo pip3 install -U pytest
 
As the task simply asked to get strings from the page using Selenium with geckodriver, on my opinion, was an overhead, I choose to use a Beautiful Soup for web scraping with Python.
It is possible to carry out web scraping tasks in many programming languages with different libraries, but using Python with the Beautiful Soup library is one of the most popular and effective methods. 
 
Beautiful Soup
https://www.crummy.com/software/BeautifulSoup/bs4/doc/
 
 pip3 install -U beautifulsoup4
 pip3 install -U html5lib
 pip3 install -U requests
 export PATH=$PATH:/home/vagrant/.local/bin
—----------------------------------------------------------------------------------------------------------
  
#4. requirements.txt file
To prepare the requirements.txt file I used pip freeze. In the current task this way with libraries is working, but for longer usage a file without mentioning the versions could be used.
 
(assignment) vagrant@waf-ubuntu-1:~/assignment$ pip freeze --all
beautifulsoup4==4.11.1
certifi==2021.10.8
charset-normalizer==2.0.12
html5lib==1.1
idna==3.3
pip==20.0.2
pkg-resources==0.0.0
requests==2.27.1
setuptools==44.0.0
six==1.16.0
soupsieve==2.3.2.post1
urllib3==1.26.9
webencodings==0.5.1
wheel==0.34.2
 
—----------------------------------------------------------------------------------------------------------
 
 
#5. Writing tests 
 
I created two separate files for each test.
One of the tests was supposed to fail according to the assignment. It is possible to write the test to succeed if the string is not found, but that wasn’t the task. 
##The solution was still added in file assignment/tests/string_not_found_not_failed.py
 
String_found.py
String_not found.py
 
The test could be written either on the local machine and pushed to git or in Web IDE and cloned to the machine.
—----------------------------------------------------------------------------------------------------------
 
#6. Reporting
 
##a. Optional report could be created as artifacts usingPytest reports
 
https://pytest-html.readthedocs.io/en/latest/user_guide.html
 
pip install pytest-html
 
to create reports call test:
 pytest --html=report.html --self-contained-html tests/string_found.py tests/string_not_found.py
 
Add to gitlab-ci.yml:
 artifacts: 
    when: always
    #paths:
      - report.html
    expire_in: 1 week
 
Also pytest-html should be added to requirements.txt
 
see pipeline run with report in artifact:
https://gitlab.com/VictoriaWells/assignment/-/pipelines/540343494
 
 
##b. Unit test reports
https://docs.gitlab.com/ee/ci/unit_test_reports.html
 
This is the option I choose, see the pipeline run:
https://gitlab.com/VictoriaWells/assignment/-/pipelines/541056527
 
 
—----------------------------------------------------------------------------------------------------------
 
#7. Git lab keys
 
View SSH client installed
ssh -V
OpenSSH_8.2p1 Ubuntu-4ubuntu0.2, OpenSSL 1.1.1f  31 Mar 2020
 
About SSH keys   
https://docs.gitlab.com/ee/user/ssh.html
 
To see if the SSH key pair already exists:
Go to your home directory.
Go to the .ssh/ subdirectory. If the .ssh/ subdirectory doesn’t exist, you are either not in the home directory, or you haven’t used ssh before. In the latter case, you need to generate an SSH key pair.
See if a file with one of the following formats exists:
cd ~
cd .ssh/
 
vagrant@waf-ubuntu-1:~/.ssh$ ll
total 16
drwx------ 2 vagrant vagrant 4096 Oct 26  2021 ./
drwxr-xr-x 6 vagrant vagrant 4096 May 15 15:14 ../
-rw------- 1 vagrant vagrant 1831 Oct 26  2021 id_rsa
-rw-r--r-- 1 vagrant vagrant  401 Oct 26  2021 id_rsa.pub
 
To Copy public key to the clipboard
xclip -sel clip < ~/.ssh/id_rsa.pub
 
Command 'xclip' can be installed with:
sudo apt install xclip
 
Paste to the key field and the key is created 
 
SSH Key
Title: vagrant@10.240.105.11
Created on: May 15, 2022 3:23pm
Expires: Never
Last used on: Never
—----------------------------------------------------------------------------------------------------------
 
#8. Created a public project in Gitlab and synchronized the repository.
 
.gitlab-ci.yml
https://dx.appirio.com/ci-cd-gitlab/gitlab-yml/
GitLab CI uses a YAML file (.gitlab-ci.yml) for project configuration. This file is placed in the root of the repository and defines the project's Pipelines, Jobs, and Environments.
 
Once committed, if no private runner were installed, a public runner is provided by Gitlab.
 
##the pipeline
At first I understood the task differently and wanted to create a runner in a docker that will run on an image in a build stage, in the test stage this docker could contain another docker to run the tests and use the created environment from the previous step. 
How to Build Docker Images In a GitLab CI Pipeline
https://www.howtogeek.com/devops/how-to-build-docker-images-in-a-gitlab-ci-pipeline/
 
The task can be interpreted differently with a pipeline containing two stages.
In the first stage the runner starts by preparing the execution environment, in the docker executor case, it pulls the image I would specify in gitlab-ci.yml and creates a container from the same image. In the next step the runner clones the git repo into it and runs the script from .gitlab-ci.yml against Python code.
 
Although it would demonstrate a deeper knowledge I believe in a simple approach and getting the task done in the most efficient way. After some clarification I decided to build a simple pipeline with a single stage that would be enough to complete the task by executing a Python test. 
It wasn't mentioned in the assignment that the pipe should be green. As the test is expected to fail, the pipeline also fails, but it is an expected behavior according to the task.
Of ourse it can be done in another way and the pipeline would be green.
 
 
https://gitlab.com/VictoriaWells/assignment
 
 
 
#Note: Although Alpine could be used. I was curious to try another build image.
Why Alpine is bad for Python applications:
https://pythonspeed.com/articles/alpine-docker-python/
Alpine images build faster and are smaller: 15 seconds instead of 30 seconds, and the image is 105MB instead of 150MB. That’s pretty good!
But when we switch to packaging a Python application, things start going wrong.
The best Docker base image for your Python application (May 2022)
https://pythonspeed.com/articles/base-image-python-docker-images/
The Python Docker image
https://hub.docker.com/_/python
Another alternative is Docker’s own “official” python image, which comes pre-installed with multiple versions of Python (3.8, 3.9, 3.10, etc.), and has multiple variants:
1.Alpine Linux.
2.Debian “Bullseye” 11, with many common packages installed. The image itself is large, but the theory is that these packages are installed via common image layers that other official Docker images will use, so overall disk usage will be low.
3.Debian 11 slim variant. This lacks the common packages’ layers, and so the image itself is much smaller, but if you use many other “official” Docker images the overall disk usage will be somewhat higher
 
 
 
 
 
 
 
